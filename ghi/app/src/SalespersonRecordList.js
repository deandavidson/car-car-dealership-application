import React, {useEffect, useState} from 'react';


function SaleHistory ( {sale, sales_record, getSale} ) {
  const [salesperson, setSalesPerson] = useState([]);
  const [salespersons, setSalespersons] = useState('')

  const handleSalespersonChange = (event) => {
      const value = event.target.value;
      setSalespersons(value);
    }

    const fetchSalespersonData = async()=> {
      const url = "http://localhost:8090/api/salesperson/"
      const response = await fetch (url)
      if (response.ok){
          const data = await response.json()
          setSalesPerson(data.salesperson)
      }
  }

    useEffect(() => {
      fetchSalespersonData();
    }, []);


    return (
      <>
      <div className="row">
        <div className="mt-3">
        <h1>Select a Salesperson!</h1>
            <div className="mb-3">
            <select onChange={handleSalespersonChange} required id="salesperson" name="salesperson" className="form-select">
                    <option value="">Choose a sales person</option>
                    {salesperson.map(salesperson => {
                      return (
                          <option title="CHOOSE YOUR FIGHTER (๑•̀ ᗝ•́ )૭" key={salesperson.id} value={salesperson.name}>{salesperson.name}</option>
                      );
                    })}
                  </select>
                  <div className="mb-3">
                    <table className="table table-striped table-hover">
                      <thead>
                        <tr>
                          <th>Sold by:</th>
                          <th>Purchased by:</th>
                          <th>VIN:</th>
                          <th>Sale Price:</th>
                        </tr>
                      </thead>
                      <tbody>
                      {sales_record.filter((sale) => sale.salesperson.name === salespersons).map(sale => {
                          return (
                            <tr key={sale}>
                            <td>{sale.salesperson.name}</td>
                            <td>{sale.customer.name}</td>
                            <td>{sale.automobile.vin}</td>
                            <td>﹩{sale.price}</td>
                            </tr>
                          )}) }
                    </tbody>

                    </table>
                </div>
            </div>
          </div>
        </div>
      </>

    )

  }

  console.log('There is one warning here because customer and salesperson both have a key called name but I would reaaaaally prefer not to mess with them and accidentally break something else along the way so for now we just have to see the ugly red block of shame whenever you click on someone please forgive me ´•︵•`')



export default SaleHistory;

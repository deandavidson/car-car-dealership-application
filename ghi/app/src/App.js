import { useState, useEffect } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SalesPersonForm from './SalesPersonForm';
import CustomerForm from './CustomerForm';
import SalesrecordForm from './SaleRecordForm';
import SaleList from './SaleRecordList';
import SaleHistory from './SalespersonRecordList';
import ManufacturerList from './ManufacturerList';
import ManufacturerForm from './ManufacturerForm';
import NewTech from './AddTech';
import TechList from './ListTechs';
import AppointmentList from './AppointmentList';
import NewAppointment from './AddAppointment';
import AppointmentSearch from './SearchVin';
import ModelsList from './ListModels';
import NewModel from './AddModel';
import AutomobilesList from './ListAutomobiles';
import NewAutomobile from './AddAutomobile';


function App(props) {

  const [salesperson, setSalesPerson] = useState ([])
  const [customer, setCustomer] = useState ([])
  const [sales_record, setSale] = useState([])
  const [manufacturer, setManufacturer] = useState ([])


  const getSalesPerson = async () => {
    const url = 'http://localhost:8090/api/salesperson/'
    const response = await fetch(url)

    if (response.ok) {
      const data = await response.json();
      const salesperson = data.salesperson
      setSalesPerson(salesperson)
      console.log(salesperson)
    }
  }

  const getCustomer = async () => {
    const url = 'http://localhost:8090/api/customer/'
    const response = await fetch(url)

    if (response.ok) {
      const data = await response.json();
      const customer = data.customer
      setCustomer(customer)
      console.log(customer)
    }
  }

  const getManufacturers = async () => {
    const url = 'http://localhost:8100/api/manufacturers/'
    const response = await fetch(url)

    if (response.ok){
      const data = await response.json();
      const manufacturers = data.manufacturers
      setManufacturer(manufacturers)
      console.log(manufacturers)
    }
  }

  const getSale = async () => {
    const url = 'http://localhost:8090/api/sale/'
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      const sales_record = data.sales_record
      setSale(sales_record)
      console.log(sales_record)
    }
  }



  useEffect(() => {
    getSalesPerson();
    getSale();
    getManufacturers();
  }, []
  )

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
        <Route path="/" element={<MainPage />} />
					<Route
						path="techs"
						element={<TechList techs={props.techs} />}
					/>
					<Route path="createtech" element={<NewTech />} />
					<Route
						path="services"
						element={<AppointmentList services={props.services} />}
					/>
					<Route path="createservice" element={<NewAppointment />} />
					<Route
						path="search"
						element={
							<AppointmentSearch services={props.services} />
						}
					/>
					<Route
						path="models"
						element={<ModelsList models={props.models} />}
					/>
					<Route path="createmodel" element={<NewModel />} />
					<Route
						path="automobiles"
						element={<AutomobilesList autos={props.autos} />}
					/>
					<Route
						path="createautomobile"
						element={<NewAutomobile />}
					/>
          <Route path="/" element={<MainPage />} />
          <Route path="salesperson/new" element={<SalesPersonForm salesperson={salesperson} getSalesPerson={getSalesPerson} />} />
          <Route path="customer/new" element={<CustomerForm customers={customer} getCustomer={getCustomer} />} />
          <Route path="sale" element={<SaleList sales={sales_record} getSale={getSale} />} />
          <Route path="sale/new" element={<SalesrecordForm sales_record={sales_record} getSale={getSale} />} />
          <Route path="sale/history" element={<SaleHistory sales_record={sales_record} salesperson={salesperson} getSale={getSale} />} />
          <Route path="manufacturer" element={<ManufacturerList manufacturers={manufacturer} getManufacturers={getManufacturers} />} />
          <Route path="manufacturer/new" element={<ManufacturerForm manufacturers={manufacturer} getManufacturers={getManufacturers} />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;


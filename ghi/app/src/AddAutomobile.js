import React, { useEffect, useState } from 'react';
import './index.css';

function NewAutomobile() {
	const [vin, setVin] = useState('');
	const [color, setcolor] = useState('');
	const [year, setyear] = useState('');
	const [model_id, setmodel_id] = useState('');
	const [models, setmodels] = useState([]);

	const handleVin = (event) => {
		const value = event.target.value;
		setVin(value);
	};
	const handlecolor = (event) => {
		const value = event.target.value;
		setcolor(value);
	};
	const handleyear = (event) => {
		const value = event.target.value;
		setyear(value);
	};

	const handlemodel_id = (event) => {
		const value = event.target.value;
		setmodel_id(value);
	};

	const handleSubmit = async (event) => {
		event.preventDefault();

		const data = {};

		data.vin = vin;
		data.color = color;
		data.year = year;
		data.model_id = model_id;

		const locationUrl = 'http://localhost:8100/api/automobiles/';
		const fetchConfig = {
			method: 'post',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json',
			},
		};

		try {
			const response = await fetch(locationUrl, fetchConfig);
			setVin('');
			setcolor('');
			setyear('');
			setmodel_id('');
		} catch (error) {
			console.log(error);
		}
	};

	const fetchData = async () => {
		const url = 'http://localhost:8100/api/models/';

		const response = await fetch(url);

		if (response.ok) {
			const data = await response.json();
			setmodels(data.models);
		}
	};

	useEffect(() => {
		fetchData();
	}, []);

	return (
		<div className="row">
			<div className="offset-3 col-6">
				<div className="shadow p-4 mt-4">
					<h1>Add a new Automobile</h1>
					<form onSubmit={handleSubmit} id="create-appointment-form">
						<div className="form-floating mb-3">
							<input
								value={vin}
								onChange={handleVin}
								placeholder="Vin"
								required
								type="text"
								name="vin"
								id="vin"
								className="form-control"
							/>
							<label htmlFor="vin">Vin Number</label>
						</div>
						<div className="form-floating mb-3">
							<input
								value={color}
								onChange={handlecolor}
								placeholder="Color"
								required
								type="text"
								name="color"
								id="color"
								className="form-control"
							/>
							<label htmlFor="color">Color</label>
						</div>
						<div className="form-floating mb-3">
							<input
								value={year}
								onChange={handleyear}
								placeholder="Year"
								required
								type="text"
								name="year"
								id="year"
								className="form-control"
							/>
							<label htmlFor="year">Year</label>
						</div>

						<div className="mb-3">
							<select
								value={model_id}
								onChange={handlemodel_id}
								required
								name="model_id"
								id="model_id"
								className="form-select"
							>
								<option value="">Choose a Model</option>
								{models.map((model) => {
									return (
										<option key={model.id} value={model.id}>
											{model.name}
										</option>
									);
								})}
							</select>
						</div>
						<button className="btn btn-primary">Create</button>
					</form>
				</div>
			</div>
		</div>
	);
}

export default NewAutomobile;

import React, { useEffect, useState } from 'react';
import './index.css';

function AppointmentSearch(props) {
	const [searchInput, setSearchInput] = useState('');
	const [History, setHistory] = useState([]);

	const services = props.services;

	function ClickIt(event) {
		event.preventDefault();
		for (let s of services) {
			if (s.vin === searchInput) {
				setHistory((History) => [...History, s]);
			}
		}
	}

	const handleChange = (e) => {
		e.preventDefault();
		setSearchInput(e.target.value);
	};

	if (searchInput.length > 0) {
		services.filter((service) => {
			const bb = service.vin.match(searchInput);
			// console.log('here', bb);
		});
	}
	console.log('this is history', History);
	return (
		<>
			<form>
				<input
					type="search"
					placeholder="Search here"
					onChange={handleChange}
					value={searchInput}
				/>
				<button onClick={ClickIt}> Search </button>
			</form>
			<table className="table table-striped">
				<thead>
					<tr>
						<th>Customer Name</th>
						<th>Vin Number</th>
						<th>Appointment Time</th>
						<th>Reason</th>
						<th>Assigned Technichian</th>
					</tr>
				</thead>
				<tbody>
					{/* {if (History.length > 0)} */}
					{History.map((service) => {
						return (
							<tr key={service.id}>
								<td>{service.customer_name}</td>
								<td>{service.vin}</td>
								<td>{service.appointment_time}</td>
								<td>{service.reason}</td>
								<td>{service.service_tech}</td>
							</tr>
						);
					})}
				</tbody>
			</table>
		</>
	);
}

export default AppointmentSearch;

import { NavLink } from 'react-router-dom';

function Nav() {
	return (
		<nav className="navbar navbar-expand-lg navbar-dark bg-success">
			<div className="container-fluid">
				<NavLink className="navbar-brand" to="/">
					CarCar
				</NavLink>
				<button
					className="navbar-toggler"
					type="button"
					data-bs-toggle="collapse"
					data-bs-target="#navbarSupportedContent"
					aria-controls="navbarSupportedContent"
					aria-expanded="false"
					aria-label="Toggle navigation"
				>
					<span className="navbar-toggler-icon"></span>
				</button>
				<div
					className="collapse navbar-collapse"
					id="navbarSupportedContent"
				>
					<ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <NavLink className="nav-link" to="/manufacturer">Manufacturer list</NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" to="/manufacturer/new">Create a manufacturer</NavLink>
              </li>
              <li className="nav-item">
							<NavLink className="nav-link" to="/models">
								List Vehicle Models
							</NavLink>
						</li>
						<li className="nav-item">
							<NavLink className="nav-link" to="/createmodel">
								Create a Vehicle Model
							</NavLink>
						</li>
            <li className="nav-item">
							<NavLink className="nav-link" to="/automobiles">
								List Autombiles
							</NavLink>
						</li>
						<li className="nav-item">
							<NavLink
								className="nav-link"
								to="/createautomobile">
								Create an Automobile
							</NavLink>
						</li>
              <li className="nav-item">
                <NavLink className="nav-link" to="/salesperson/new">Add a salesperson</NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" to="/customer/new">Add a potential customer</NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" to="/sale">List of all sales</NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" to="/sale/history">View salesperson's history</NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" to="/sale/new">Create a sale record</NavLink>
              </li>
            <li className="nav-item">
							<NavLink className="nav-link" to="/techs">
								Technichian List
							</NavLink>
						</li>
						<li className="nav-item">
							<NavLink className="nav-link" to="/createtech">
								Create Tech
							</NavLink>
						</li>
						<li className="nav-item">
							<NavLink className="nav-link" to="/createservice">
								Create Service
							</NavLink>
						</li>

						<li className="nav-item">
							<NavLink className="nav-link" to="/services">
								Services List
							</NavLink>
						</li>
						<li className="nav-item">
							<NavLink className="nav-link" to="/search">
								Search Vin
							</NavLink>
						</li>


					</ul>
				</div>
			</div>
		</nav>
	);
}

export default Nav;

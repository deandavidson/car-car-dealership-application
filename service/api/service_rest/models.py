from django.db import models
from django.urls import reverse

class ServiceTechnicianVO(models.Model):
    tech_name = models.CharField(max_length=200)
    tech_number = models.PositiveSmallIntegerField()

    def __str__(self):
        return self.tech_name



class ServiceAppointment(models.Model):
    vin = models.CharField(max_length=200)
    customer_name = models.CharField(max_length=200)
    appointment_time = models.DateTimeField()
    reason = models.CharField(max_length=200)
    service_tech = models.ForeignKey(
        ServiceTechnicianVO,
        related_name="service_tech",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.vin


class ManufacturerVO(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def get_api_url(self):
        return reverse("api_manufacturer", kwargs={"pk": self.id})


class VehicleModelVO(models.Model):
    name = models.CharField(max_length=100)
    picture_url = models.URLField()

    manufacturer = models.ForeignKey(
        ManufacturerVO,
        related_name="models",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_vehicle_model", kwargs={"pk": self.id})


class AutomobileVO(models.Model):
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=17, unique=True)

    model = models.ForeignKey(
        VehicleModelVO,
        related_name="automobiles",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_automobile", kwargs={"vin": self.vin})

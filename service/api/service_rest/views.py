from django.shortcuts import render
from .models import ServiceAppointment, ServiceTechnicianVO
import json
from django.http import JsonResponse
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods


class ServiceTechnicianEncoder(ModelEncoder):
    model = ServiceTechnicianVO
    properties = [
        "id",
        "tech_name",
        "tech_number",
    ]



class ServiceAppointmentDetailEncoder(ModelEncoder):
    model = ServiceAppointment
    properties = [
        "id",
        "vin",
        "customer_name",
        "appointment_time",
        "reason",
    ]
    encoders = {"service_tech": ServiceTechnicianEncoder}

class ServiceAppointmentListEncoder(ModelEncoder):
    model = ServiceAppointment
    properties = [
        "id",
        "vin",
        "customer_name",
        "appointment_time",
        "reason",
    ]
    def get_extra_data(self, o):
        return {"service_tech": o.service_tech.tech_name}

@require_http_methods(["GET", "POST"])
def list_services(request):
    if request.method == "GET":
        # if servicetech_vo_id is not None:
        #     services = ServiceAppointment.objects.filter(service_tech=servicetech_vo_id)
        # else:
        services = ServiceAppointment.objects.all()
        return JsonResponse(
            {"services": services},
            encoder=ServiceAppointmentListEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            id = content["service_tech"]
            tech_name = ServiceTechnicianVO.objects.get(id=id)
            content["service_tech"] = tech_name
        except ServiceTechnicianVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid tech"},
                status=400
            )
        services = ServiceAppointment.objects.create(**content)
        return JsonResponse(
            services,
            encoder = ServiceAppointmentDetailEncoder,
            safe=False
        )

@require_http_methods(["GET", "DELETE"])
def show_service(request, id):
    if request.method == "GET":
        service = ServiceAppointment.objects.get(id=id)
        return JsonResponse(
            service,
            encoder=ServiceAppointmentDetailEncoder,
            safe=False
        )
    else:
        count, _ = ServiceAppointment.objects.filter(id=id).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )


@require_http_methods(["GET", "POST", "DELETE"])
def create_tech(request):
    if request.method == "GET":
        techs = ServiceTechnicianVO.objects.all()
        return JsonResponse(
            {"techs": techs},
            encoder=ServiceTechnicianEncoder
        )
    # elif request.method == "DELETE":
    #     count, _ = ServiceTechnicianVO.objects.filter(id=id).delete()
    #     return JsonResponse(
    #         {"deleted": count > 0}
    #     )
    else:
        content = json.loads(request.body)
        # try:
        #     id = content["tech_number"]
        #     tech_number = ServiceTechnicianVO.objects.get(id=id)
        #     content["tech_number"] = tech_number
        # except ServiceTechnicianVO.DoesNotExist:
        #     return JsonResponse(
        #         {"message": "Invalid tech"},
        #         status=400
        #     )
        tech = ServiceTechnicianVO.objects.create(**content)
        return JsonResponse(
            tech,
            encoder = ServiceTechnicianEncoder,
            safe=False
        )


@require_http_methods(["GET", "DELETE"])
def show_tech(request, id):
    if request.method == "GET":
        service = ServiceTechnicianVO.objects.get(id=id)
        return JsonResponse(
            service,
            encoder=ServiceTechnicianEncoder,
            safe=False
        )
    else:
        count, _ = ServiceTechnicianVO.objects.filter(id=id).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )
